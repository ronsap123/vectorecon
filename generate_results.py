import matplotlib.pyplot as plt
import vectorecon as vr
import numpy as np


f = open('config.json')
data = f.read()
f.close()

props = eval(data)

nr = vr.NaiveVectorReconstructor()
fcr = vr.FullyConnectedVectorReconstructor()
afcr = vr.AdvancedFullyConnectedVectorReconstructor()

LEARNING_RATE = props["learning-rate"]
MAX_SCALE = props["max-sigma-scale"]
BATCH_SIZE = props["batch-size"]
NO_EPOCHS = props["epochs"]
VECTOR_SIZE = props["size"]

print(LEARNING_RATE,MAX_SCALE,BATCH_SIZE,NO_EPOCHS,VECTOR_SIZE)

fcr.initiate(VECTOR_SIZE) #this opens the session for the neural network

#now we train the neural networks
fcr.train(LEARNING_RATE,NO_EPOCHS,BATCH_SIZE,MAX_SCALE)

afcr.initiate(VECTOR_SIZE)
afcr.train(LEARNING_RATE,NO_EPOCHS,BATCH_SIZE,MAX_SCALE)

nr_errs   = []
fcr_errs  = []
afcr_errs = []

for scale in range(1,100):
    print(scale,'%')
    nr_err_sum  = 0
    fcr_err_sum = 0
    afcr_err_sum = 0
    for i in range(0,200):
        vector = np.ones((VECTOR_SIZE))*0
        vector_noise = np.random.normal(scale=scale,size=VECTOR_SIZE)
        fvector = vector+vector_noise
        resnr = nr.reconstruct(fvector)
        resfcr = fcr.reconstruct(fvector)
        resafcr = afcr.reconstruct(fvector)
        nr_err_sum  += resnr
        fcr_err_sum += resfcr
        afcr_err_sum += resafcr
        
        vector = np.ones((VECTOR_SIZE))*1
        vector_noise = np.random.normal(scale=scale,size=VECTOR_SIZE)
        fvector = vector+vector_noise
        resnr = nr.reconstruct(fvector)
        resfcr = fcr.reconstruct(fvector)
        resafcr = afcr.reconstruct(fvector)
        nr_err_sum  += 1-resnr
        fcr_err_sum += 1-resfcr
        afcr_err_sum += 1-resafcr
    nr_err_sum  /= 400.0
    fcr_err_sum /= 400.0
    afcr_err_sum /= 400.0
    
    nr_errs.append(nr_err_sum)
    fcr_errs.append(fcr_err_sum)
    afcr_errs.append(afcr_err_sum)

plt.plot(nr_errs,   label="naive")
plt.plot(fcr_errs,  label="fully connected NN")
plt.plot(afcr_errs, label="adv fully connected NN")
plt.xlabel('Gaussian Distribution Noise Scale')
plt.ylabel('Average Error At Scale')
plt.legend()
plt.savefig('result')
plt.show()

fcr.close()
afcr.close()