import numpy as np
import tensorflow as tf
from abc import ABC, abstractmethod
import random


'''
this function will return a random set of vectors with the desired output for each
'''
def get_batch(batch_size, size, scale):
    inps = []
    outs = []
    
    for i in range(0,int(batch_size/2)):
        vector = np.ones((size))*0
        vector_noise = np.random.normal(scale=scale,size=size)
        inps.append(vector+vector_noise)
        outs.append([0])
        
        vector = np.ones((size))*1
        vector_noise = np.random.normal(scale=scale,size=size)
        inps.append(vector+vector_noise)
        outs.append([1])
    return np.array(inps),np.array(outs)


'''
BASE ABSTRACT CLASS
each reconstructor will have a reconstruct function that recieves a vector and return either a one or a zero
'''
class VectorReconstructor(ABC):
    def __init__(self):
        pass
    
    @abstractmethod
    def reconstruct(self, vector):
        pass


'''
NAIVE RECONSTRUCTOR
uses a simple numpy logic statement to try and reconstruct the original vector
'''
class NaiveVectorReconstructor(VectorReconstructor):

    def reconstruct(self, vector):
        '''
        The naive approach I took was counting the amount of values that are within 0.5
        distance to 0 and the amount of values that withing 0.5 distance to 1 and see 
        which do I have more of and decide.
        '''
        res_vals = vector[np.where(np.logical_and(vector > -0.5, vector < 0.5))].shape[0],vector[np.where(np.logical_and(vector > 0.5, vector < 1.5))].shape[0]
        if res_vals[0] > res_vals[1]:
            return 0
        else:
            return 1

'''
FULLY CONNECTED RECONSTRUCTOR
this reconstructor uses and trains a neural network to reconstruct vectors
'''
class FullyConnectedVectorReconstructor(VectorReconstructor):
    def __init__(self):
        self.initiated = False
        self.trained = False
        
    def initiate(self, size):
        self.size = size
        
        tf.reset_default_graph()
        self.input = tf.placeholder(dtype=tf.float32, shape=[None,size])
        self.desout = tf.placeholder(dtype=tf.float32, shape=[None,1])
        
        self.learning_rate = tf.placeholder(dtype=tf.float32, shape=None)
        
        first_layer = min(size*0.8,128)
        self.fc1 = tf.layers.dense(self.input, int(first_layer), activation=tf.nn.relu)
        self.fc2 = tf.layers.dense(self.fc1, int(first_layer*0.6), activation=tf.nn.relu)
        self.fc3 = tf.layers.dense(self.fc2, int(first_layer*0.4), activation=tf.nn.relu)
        self.fc4 = tf.layers.dense(self.fc3, int(first_layer*0.2), activation=tf.nn.relu)
        self.fc5 = tf.layers.dense(self.fc4, int(first_layer*0.2), activation=tf.nn.sigmoid)
        self.out = tf.layers.dense(self.fc5, 1, activation=tf.nn.sigmoid)
        
        self.cost = tf.reduce_mean(tf.square(tf.subtract(self.out,self.desout)))
        
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.cost)
        
        
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        
        self.initiated = True
        pass
        
    def train(self, learning_rate, epochs, batch_size, scale_range):
        summ = 0
        for i in range(0, epochs):
            inps, outs = get_batch(batch_size, self.size, random.randrange(0,scale_range))
            
            _, cost = self.sess.run([self.optimizer, self.cost], feed_dict={self.input:inps,self.desout:outs,self.learning_rate:learning_rate})
            summ += cost
            if i % 10 == 0:
            	print('EPOCH:',i,'\tCOST:\t',summ/10.0)
            	summ = 0
        
        self.trained = True
        pass
    
    def reconstruct(self,vector):
        
        if not self.initiated:
            raise Exception("Neural Network not initialized")
        
        if not self.trained:
            print("Warning, reconstructor untrained")
        
        if vector.shape[0] != self.size:
            raise Exception("Size doesn't match the size initialized")
        
        res = self.sess.run(self.out, feed_dict={self.input:[vector]})
        
        res = res[0]
        
        if res > 0.5:
            return 1
        else:
            return 0
    
    def close(self):
        self.sess.close()


'''
ADVANCED FULLY CONNECTED RECONSTRUCTOR
this reconstructor simply drops all values that are outside the range of -0.5 - 1.5 in the vector and trains the neural network using only the remaining values
'''
class AdvancedFullyConnectedVectorReconstructor(VectorReconstructor):
    def __init__(self):
        self.initiated = False
        self.trained = False
        
    def initiate(self, size):
        self.size = size
        
        tf.reset_default_graph()
        self.input = tf.placeholder(dtype=tf.float32, shape=[None,size])
        self.desout = tf.placeholder(dtype=tf.float32, shape=[None,1])
        
        self.learning_rate = tf.placeholder(dtype=tf.float32, shape=None)
        first_layer = min(size *0.8,128)
        self.fc1 = tf.layers.dense(self.input, int(first_layer), activation=tf.nn.relu)
        self.fc2 = tf.layers.dense(self.fc1, int(first_layer*0.6), activation=tf.nn.relu)
        self.fc3 = tf.layers.dense(self.fc2, int(first_layer*0.4), activation=tf.nn.relu)
        self.fc4 = tf.layers.dense(self.fc3, int(first_layer*0.2), activation=tf.nn.relu)
        self.fc5 = tf.layers.dense(self.fc4, int(first_layer*0.2), activation=tf.nn.sigmoid)
        self.out = tf.layers.dense(self.fc5, 1, activation=tf.nn.sigmoid)
        
        self.cost = tf.reduce_mean(tf.square(tf.subtract(self.out,self.desout)))
        
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.cost)
        
        
        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())
        
        self.initiated = True
        pass
        
    def train(self, learning_rate, epochs, batch_size, scale_range):
        self.trained = True
        summ = 0
        for i in range(0, epochs):
            inps, outs = get_batch(batch_size, self.size, random.randrange(0,scale_range))
            inps[np.where(np.logical_and(inps > -0.5, inps < 1.5))] = -1
            
            _, cost = self.sess.run([self.optimizer, self.cost], feed_dict={self.input:inps,self.desout:outs,self.learning_rate:learning_rate})
            summ += cost
            if i % 10 == 0:
            	print('EPOCH:',i,'\tCOST:\t',summ/10.0)
            	summ = 0
        
        pass
    
    def reconstruct(self,vector):
        
        if not self.initiated:
            raise Exception("Neural Network not initialized")
        
        if not self.trained:
            print("Warning, reconstructor untrained")
        
        if vector.shape[0] != self.size:
            raise Exception("Size doesn't match the size initialized")
        
        vector[np.where(np.logical_and(vector > -0.5, vector < 1.5))] = -1
        res = self.sess.run(self.out, feed_dict={self.input:[vector]})
        
        res = res[0]
        
        if res > 0.5:
            return 1
        else:
            return 0
    
    def close(self):
        self.sess.close()